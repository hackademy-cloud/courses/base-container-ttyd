FROM debian

RUN apt update && apt upgrade -yy && apt install -yy build-essential cmake git libjson-c-dev libwebsockets-dev
RUN git clone https://github.com/tsl0922/ttyd.git && \
    cd ttyd && mkdir build && cd build && \
    cmake .. && \
    make && make install
RUN apt install nano

WORKDIR /h4cker

CMD ["ttyd", "-p", "8080", "/bin/bash"]

EXPOSE 8080
